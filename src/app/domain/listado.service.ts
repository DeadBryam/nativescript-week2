import { Injectable } from "@angular/core";

@Injectable()
export class ListadoService {
    private listado: Array<String> = [];
    private puntuaciones: Array<Puntuacion> = [];

    add(item: String): void {
        this.listado.push(item);
    }

    clear(): void {
        this.listado = [];
    }

    delete(index: number): void {
        this.listado.splice(index, 1);
    }

    findAll(): Array<String> {
        return this.listado;
    }
    
    update(index:number,value:string):void {
        console.log(this.listado); 
        console.log(index); 
        this.listado.splice(index, 1, value);
        console.log(this.listado); 
    }

    addRandomPuntuacion(): void {
        this.puntuaciones.push({
            imagen: "res://icon",
            nombre: "Random",
            puntuacion: (Math.random() * 100).toFixed()
        });
    }

    findAllPuntuaciones() {
        return this.puntuaciones;
    }

    deletePuntuacion(index: number): void {
        this.puntuaciones.splice(index, 1);
    }
}

export class Puntuacion {
    imagen: String;
    nombre: String;
    puntuacion: String;
}
