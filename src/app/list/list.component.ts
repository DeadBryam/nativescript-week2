import {
    Component,
    OnInit,
    ViewContainerRef,
    ViewChild,
    ElementRef
} from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { ListadoService } from "../domain/listado.service";
import { RouterExtensions } from "nativescript-angular/router";
import { isAndroid } from "tns-core-modules/ui/page/page";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toast";
import { View, Color } from "tns-core-modules/ui/core/view/view";
import {
    ModalDialogService,
    ModalDialogOptions
} from "nativescript-angular/common";
import { UpdateModalComponent } from "./update/list-update.component";

@Component({
    selector: "List",
    templateUrl: "./list.component.html",
    providers: [ModalDialogService],
    styleUrls: ["./list.component.scss"]
})
export class ListComponent implements OnInit {
    listHeader: String;
    resultados: Array<String>;
    @ViewChild("layout", { static: true }) layout: ElementRef;
    @ViewChild("list", { static: true }) list: ElementRef;

    constructor(
        private listado: ListadoService,
        private _routerExtensions: RouterExtensions,
        private modalService: ModalDialogService,
        private viewContainerRef: ViewContainerRef
    ) {
        this.listHeader = "List";

        for (let i: number = 0; i < 20; i++) {
            this.listado.add((Math.random() * 1000).toFixed());
        }

        this.resultados = this.listado.findAll();
    }

    ngOnInit(): void {
        if (isAndroid) {
            this.listHeader = "Hello Android :)";
        }
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args: string): void {
        dialogs
            .action("Que desea hacer?", "Cancelar", ["Editar", "Ver detalles"])
            .then(result => {
                if (result == "Editar") {
                    this.showModal(args);
                } else if (result == "Ver detalles") {
                    Toast.makeText("OK boomer").show();
                    this.changePage("/list/detalle");
                }
            });
    }

    search(args: string): void {
        const element = <View>this.layout.nativeElement;
        const list = <View>this.list.nativeElement;

        element
            .animate({
                duration: 500,
                delay: 0,
                backgroundColor: new Color("blue")
            })
            .then(_ => {
                element.animate({
                    duration: 500,
                    delay: 0,
                    backgroundColor: new Color("white")
                });
            });

        list.animate({
            duration: 500,
            delay: 0,
            opacity: 0.0
        }).then(_ => {
            list.animate({
                duration: 500,
                delay: 0,
                opacity: 1.0
            });
        });

        this.resultados = this.listado.findAll().filter(x => x.includes(args));
    }

    changePage(page: string) {
        this._routerExtensions.navigate([page], {
            transition: {
                name: "fade"
            }
        });
    }

    showModal(value: string) {
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: false,
            context: {
                value: value
            },
            animated: true
        };
        this.modalService.showModal(UpdateModalComponent, options);
    }
}
